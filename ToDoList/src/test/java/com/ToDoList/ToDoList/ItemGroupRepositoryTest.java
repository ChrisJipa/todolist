package com.ToDoList.ToDoList;


import com.ToDoList.ToDoList.model.Item;
import com.ToDoList.ToDoList.model.ItemGroup;
import com.ToDoList.ToDoList.model.ItemStatus;
import com.ToDoList.ToDoList.repositories.ItemGroupRepository;
import com.ToDoList.ToDoList.repositories.ItemRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;


@RunWith(SpringRunner.class)
@DataJpaTest
public class ItemGroupRepositoryTest {

    @Autowired
    private ItemGroupRepository itemGroupRepository;

    @Autowired
    private ItemRepository itemRepository;

    @Test
    public void testCreateItemGroupRepository() {
        Item item1 = new Item("item1", "item1_description", ItemStatus.DONE);
        Item item2 = new Item("item2", "item2_description", ItemStatus.DONE);
        ItemGroup itemGroup = new ItemGroup();
        List<Item> itemList = new ArrayList<>();
        itemList.add(item1);
        itemList.add(item2);
        itemGroup.setItemGroupName("Item_Group_Name");
        itemGroup.setItemList(itemList);
        ItemGroup savedItemGroup = itemGroupRepository.save(itemGroup);

        assertNotNull(savedItemGroup.getId());
        assertEquals("Item_Group_Name", savedItemGroup.getItemGroupName());
        assertEquals(itemList.size(), savedItemGroup.getItemList().size());
    }

    @Test
    public void testUpdateItemFromItemGroup() {
        Item item1 = new Item("item1", "item1_description", ItemStatus.IN_PROGRESS);
        Item item2 = new Item("item2", "item2_description", ItemStatus.IN_PROGRESS);
        ItemGroup itemGroup = new ItemGroup();
        List<Item> itemList = new ArrayList<>();
        itemList.add(item1);
        itemList.add(item2);
        itemGroup.setItemGroupName("Item_Group_Name");
        itemGroup.setItemList(itemList);
        ItemGroup savedItemGroup = itemGroupRepository.save(itemGroup);

        savedItemGroup
                .getItemList()
                .forEach(item -> {
                    item.setItemStatus(ItemStatus.DONE);
                    itemRepository.save(item);
                });

        ItemGroup itemGroupFromDB = itemGroupRepository.findById(savedItemGroup.getId()).get();

        itemGroupFromDB
                .getItemList()
                .forEach(item -> {
                    assertEquals(ItemStatus.DONE, item.getItemStatus());
                });
    }

    @Test
    public void testDeleteItemGroup() {
        Item item1 = new Item("item1", "item1_description", ItemStatus.DONE);
        Item item2 = new Item("item2", "item2_description", ItemStatus.DONE);
        ItemGroup itemGroup = new ItemGroup();
        List<Item> itemList = new ArrayList<>();
        itemList.add(item1);
        itemList.add(item2);
        itemGroup.setItemGroupName("Item_Group_Name");
        itemGroup.setItemList(itemList);
        ItemGroup savedItemGroup = itemGroupRepository.save(itemGroup);

        itemGroupRepository.delete(savedItemGroup);

        Optional<ItemGroup> deletedItemGroup = itemGroupRepository.findById(savedItemGroup.getId());
        assertFalse(deletedItemGroup.isPresent());

        long itemRepositoryCount = itemRepository.count(); //counting number of items in DB
        assertEquals(0, itemRepositoryCount);
    }
}
