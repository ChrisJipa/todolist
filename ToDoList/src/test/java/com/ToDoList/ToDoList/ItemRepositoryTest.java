package com.ToDoList.ToDoList;

import com.ToDoList.ToDoList.model.Item;
import com.ToDoList.ToDoList.model.ItemStatus;
import com.ToDoList.ToDoList.repositories.ItemRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ItemRepositoryTest {

    @Autowired
    private ItemRepository itemRepository;

    @Test
    public void testCreateOfItem() {
        Item item = new Item("ItemName", "ItemDescription", ItemStatus.NOT_STARTED);
        Item dbItem = itemRepository.save(item);
        assertNotNull(dbItem.getId());
    }

    @Test
    public void testItemFindByID() {
        Item item = new Item("ItemName", "ItemDescription", ItemStatus.NOT_STARTED);
        Item dbItem = itemRepository.save(item);
        assertNotNull(itemRepository.findById(dbItem.getId()));
    }

    @Test
    public void testUpdateOfItem() {
        Item item = new Item("ItemName", "ItemDescription", ItemStatus.NOT_STARTED);
        Item dbItem = itemRepository.save(item);
        dbItem.setItemStatus(ItemStatus.IN_PROGRESS);
        Item updatedItem = itemRepository.save(dbItem);
        assertEquals(ItemStatus.IN_PROGRESS, updatedItem.getItemStatus());
    }

    @Test
    public void testDeleteItem() {
        Item item = new Item("ItemName", "ItemDescription", ItemStatus.NOT_STARTED);
        Item dbItem = itemRepository.save(item);
        itemRepository.deleteById(dbItem.getId());
        assertEquals(0, itemRepository.count());
    }

    @Test
    public void testFindAllItems() {
        Item item = new Item("ItemName", "ItemDescription", ItemStatus.NOT_STARTED);
        Item item2 = new Item("ItemName", "ItemDescription", ItemStatus.NOT_STARTED);
        itemRepository.save(item);
        itemRepository.save(item2);
        assertEquals(2, itemRepository.count());
    }

    @Test
    public void findAllByItemStatus() {
        Item item = new Item("ItemName", "ItemDescription", ItemStatus.NOT_STARTED);
        Item item2 = new Item("ItemName", "ItemDescription", ItemStatus.DONE);
        Item item3 = new Item("ItemName", "ItemDescription", ItemStatus.DONE);
        itemRepository.save(item);
        itemRepository.save(item2);
        itemRepository.save(item3);
        assertEquals(2, itemRepository.findAllByItemStatus(ItemStatus.DONE).size());
    }

}
