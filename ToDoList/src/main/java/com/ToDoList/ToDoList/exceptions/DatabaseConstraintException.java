package com.ToDoList.ToDoList.exceptions;

public class DatabaseConstraintException extends RuntimeException{
    public DatabaseConstraintException(String message) {
        super (message);
    }
}
