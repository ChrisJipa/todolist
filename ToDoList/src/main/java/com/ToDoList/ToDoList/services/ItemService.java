package com.ToDoList.ToDoList.services;

import com.ToDoList.ToDoList.model.Item;
import com.ToDoList.ToDoList.model.ItemStatus;

import java.util.List;

public interface ItemService {

    Item save(Item item);

    Item findById(Long id);

    void delete(Long id);

    List<Item> findAllItemsByStatus(ItemStatus itemStatus);

    List<Item> findAll();

}
