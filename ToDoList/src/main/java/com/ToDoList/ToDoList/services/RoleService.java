package com.ToDoList.ToDoList.services;

import com.ToDoList.ToDoList.model.Role;

import java.util.List;

public interface RoleService {

    Role create(Role role);
    Role findByRoleName(String roleName);
    List<Role> findAll();
}
