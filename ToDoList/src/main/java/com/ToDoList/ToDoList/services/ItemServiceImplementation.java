package com.ToDoList.ToDoList.services;

import com.ToDoList.ToDoList.exceptions.ResourceNotFoundException;
import com.ToDoList.ToDoList.model.Item;
import com.ToDoList.ToDoList.model.ItemStatus;
import com.ToDoList.ToDoList.repositories.ItemRepository;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class ItemServiceImplementation implements ItemService {

    private ItemRepository itemRepository;

    public ItemServiceImplementation(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }

    @Override
    public Item save(Item item) {
        return itemRepository.save(item);
    }

    @Override
    public Item findById(Long id) {
        Optional<Item> optionalItem =  itemRepository.findById(id);
        if (optionalItem.isPresent()) {
            return optionalItem.get();
        } else {
            throw new ResourceNotFoundException(String.format("Resource item with id %d was not found", id));
        }
    }

    @Override
    public void delete(Long id) {
        itemRepository.deleteById(id);

    }

    @Override
    public List<Item> findAllItemsByStatus(ItemStatus itemStatus) {
        return itemRepository.findAllByItemStatus(itemStatus);
    }

    @Override
    public List<Item> findAll() {
        return itemRepository.findAll();
    }
}
