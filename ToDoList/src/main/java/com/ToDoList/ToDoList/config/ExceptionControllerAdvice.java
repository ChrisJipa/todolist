package com.ToDoList.ToDoList.config;


import com.ToDoList.ToDoList.exceptions.DatabaseConstraintException;
import com.ToDoList.ToDoList.exceptions.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ExceptionControllerAdvice {

    @ExceptionHandler(value = {ResourceNotFoundException.class})
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public ErrorMessage resourceNotFoundException(ResourceNotFoundException resourceNotFoundException) {
        ErrorMessage errorMessage = new ErrorMessage();
        errorMessage.setMessage(resourceNotFoundException.getMessage());
        return errorMessage;
    }

    @ExceptionHandler(value = {DatabaseConstraintException.class})
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ErrorMessage dataBaseException(DatabaseConstraintException databaseConstraintException) {
        ErrorMessage errorMessage = new ErrorMessage();
        errorMessage.setMessage(databaseConstraintException.getMessage());
        return errorMessage;
    }

}
