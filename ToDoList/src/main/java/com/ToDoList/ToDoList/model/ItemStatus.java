package com.ToDoList.ToDoList.model;

public enum ItemStatus {
    NOT_STARTED, IN_PROGRESS, DONE
}
