package com.ToDoList.ToDoList.repositories;

import com.ToDoList.ToDoList.model.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long> {
    User findByUsername(String username);
}
