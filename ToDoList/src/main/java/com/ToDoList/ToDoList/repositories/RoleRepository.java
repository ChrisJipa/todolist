package com.ToDoList.ToDoList.repositories;

import com.ToDoList.ToDoList.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

public interface RoleRepository extends JpaRepository<Role, Long> {
    Role findByRole(String roleName);
}
