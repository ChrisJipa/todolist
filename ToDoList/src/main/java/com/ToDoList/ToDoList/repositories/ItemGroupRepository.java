package com.ToDoList.ToDoList.repositories;

import com.ToDoList.ToDoList.model.ItemGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ItemGroupRepository extends JpaRepository <ItemGroup, Long>{

}
