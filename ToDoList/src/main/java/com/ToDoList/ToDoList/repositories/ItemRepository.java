package com.ToDoList.ToDoList.repositories;

import com.ToDoList.ToDoList.model.Item;
import com.ToDoList.ToDoList.model.ItemStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public interface ItemRepository extends JpaRepository<Item, Long> {

    List<Item> findAllByItemStatus(ItemStatus itemStatus);
}
