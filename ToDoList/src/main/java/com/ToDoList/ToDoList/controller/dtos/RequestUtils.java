package com.ToDoList.ToDoList.controller.dtos;

import com.ToDoList.ToDoList.model.Item;
import com.ToDoList.ToDoList.model.ItemGroup;

import java.util.ArrayList;
import java.util.List;

public class RequestUtils {

    public static Item mapItemDtoToItem(RequestItemDto itemDto) {
        return new Item(itemDto.getName(), itemDto.getDescription(), itemDto.getItemStatus());
    }

    public static ItemGroup mapItemGroupDtoToItemGroup(RequestItemGroupDto requestItemGroupDto) {
        ItemGroup itemGroup = new ItemGroup();
        List<Item> itemList = new ArrayList<>();

        itemGroup.setItemGroupName(requestItemGroupDto.getRequestItemGroupDtoName());

        requestItemGroupDto
                .getRequestItemDtoList()
                .forEach(requestItemDto -> itemList.add(mapItemDtoToItem(requestItemDto)));
        itemGroup.setItemList(itemList);

        return itemGroup;
    }
}
