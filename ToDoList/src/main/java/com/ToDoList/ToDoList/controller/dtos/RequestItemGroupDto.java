package com.ToDoList.ToDoList.controller.dtos;

import java.util.List;

public class RequestItemGroupDto {

    private String requestItemGroupDtoName;
    private List<RequestItemDto> requestItemDtoList;

    public String getRequestItemGroupDtoName() {
        return requestItemGroupDtoName;
    }

    public void setRequestItemGroupDtoName(String requestItemGroupDtoName) {
        this.requestItemGroupDtoName = requestItemGroupDtoName;
    }

    public List<RequestItemDto> getRequestItemDtoList() {
        return requestItemDtoList;
    }

    public void setRequestItemDtoList(List<RequestItemDto> requestItemDtoList) {
        this.requestItemDtoList = requestItemDtoList;
    }
}
