package com.ToDoList.ToDoList.controller;

import com.ToDoList.ToDoList.controller.dtos.*;
import com.ToDoList.ToDoList.model.ItemGroup;
import com.ToDoList.ToDoList.services.ItemGroupService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(path = "/itemGroups")
public class ItemGroupController {

    private ItemGroupService itemGroupService;

    public ItemGroupController(ItemGroupService itemGroupService) {
        this.itemGroupService = itemGroupService;
    }

    @PostMapping
    public ResponseItemGroupDto save(@RequestBody RequestItemGroupDto requestItemGroupDto) {
        ItemGroup itemGroup = this.itemGroupService.save(RequestUtils.mapItemGroupDtoToItemGroup(requestItemGroupDto));
        return mapItemGroupToResponseItemGroupDto(itemGroup);
    }

    @GetMapping()
    public List<ResponseItemGroupDto> findAll() {
        return mapItemGroupListToResponseItemGroupDtoList(this.itemGroupService.findAll());
    }

    @GetMapping(path = "/{id}")
    public ResponseItemGroupDto findById(@PathVariable("id") Long id) {
        return mapItemGroupToResponseItemGroupDto(itemGroupService.findById(id));
    }

    @DeleteMapping(path = "/{id}")
    public HttpStatus deleteById(@PathVariable("id") Long id) {
        itemGroupService.delete(id);
        return HttpStatus.OK;
    }

    private ResponseItemGroupDto mapItemGroupToResponseItemGroupDto(ItemGroup itemGroup) {
        ResponseItemGroupDto responseItemGroupDto = new ResponseItemGroupDto();
        responseItemGroupDto.setId(itemGroup.getId());
        responseItemGroupDto.setItemGroupName(itemGroup.getItemGroupName());
        responseItemGroupDto
                .setResponseItemDtoList(ResponseUtils.mapItemListToResponseItemDtoList(itemGroup.getItemList()));

        return responseItemGroupDto;
    }

    private List<ResponseItemGroupDto> mapItemGroupListToResponseItemGroupDtoList(List<ItemGroup> itemGroupList) {
        List<ResponseItemGroupDto> responseItemGroupDtoList = new ArrayList<>();
        itemGroupList.forEach(itemGroup -> {
            ResponseItemGroupDto responseItemGroupDto = mapItemGroupToResponseItemGroupDto(itemGroup);
            responseItemGroupDtoList.add(responseItemGroupDto);
        });
        return responseItemGroupDtoList;
    }

}
