package com.ToDoList.ToDoList.controller.dtos;

import com.ToDoList.ToDoList.model.Item;
import com.ToDoList.ToDoList.model.ItemStatus;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class ResponseUtils {

    public static ResponseItemDto mapItemToResponseItemDto(Item item) {
        ResponseItemDto responseItemDto
                = new ResponseItemDto(item.getId(), item.getName(), item.getDescription(), item.getItemStatus());
        if (item.getItemGroup() != null) {
            responseItemDto.setAssignedToItemGroupId(item.getItemGroup().getId());
            responseItemDto.setAssignedToItemGroupName(item.getItemGroup().getItemGroupName());
        }
        if (item.getItemStatus().equals(ItemStatus.DONE)) {
            long diffInMilliseconds = item.getEndDate().getTime() - item.getStartDate().getTime();
            long diffInMinutes = TimeUnit.MINUTES.convert(diffInMilliseconds, TimeUnit.MILLISECONDS);
            responseItemDto.setDuration(String.format("Took %d minute(s)", diffInMinutes));
        }
        return responseItemDto;
    }

    public static List<ResponseItemDto> mapItemListToResponseItemDtoList(List<Item> itemsList) {
        List<ResponseItemDto> responseItemsDtoList = new ArrayList<>();
        itemsList.forEach(item -> {
            ResponseItemDto responseItemDto = mapItemToResponseItemDto(item);
            responseItemsDtoList.add(responseItemDto);
        });
        return responseItemsDtoList;
    }
}
