package com.ToDoList.ToDoList.controller;

import com.ToDoList.ToDoList.controller.dtos.RequestItemDto;
import com.ToDoList.ToDoList.controller.dtos.RequestUtils;
import com.ToDoList.ToDoList.controller.dtos.ResponseItemDto;
import com.ToDoList.ToDoList.controller.dtos.ResponseUtils;
import com.ToDoList.ToDoList.model.Item;
import com.ToDoList.ToDoList.model.ItemStatus;
import com.ToDoList.ToDoList.services.ItemService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
public class ItemController {

    private ItemService itemService;

    public ItemController(ItemService itemService) {
        this.itemService = itemService;
    }

    @PostMapping("/items")
    public ResponseItemDto create(@RequestBody RequestItemDto itemDto) {
        Item itemToBeSaved = RequestUtils.mapItemDtoToItem(itemDto);
        return ResponseUtils.mapItemToResponseItemDto(itemService.save(itemToBeSaved));
    }

    @GetMapping("/items")
    public List<ResponseItemDto> getAll() {
        return ResponseUtils.mapItemListToResponseItemDtoList(itemService.findAll());
    }

    @GetMapping("/items/{id}")
    public ResponseItemDto findItemById(@PathVariable("id") Long id) {
        return ResponseUtils.mapItemToResponseItemDto(itemService.findById(id));
    }

    @PutMapping("/items/{id}")
    public ResponseItemDto updateItem(@PathVariable("id") Long id, @RequestBody RequestItemDto requestItemDto) {
        Item updatedItem = updateItemFromRequestItemDto(itemService.findById(id), requestItemDto);
        return ResponseUtils.mapItemToResponseItemDto(itemService.save(updatedItem));
    }

    @DeleteMapping(path = "/items/{id}")
    public HttpStatus deleteItem(@PathVariable("id") Long id) {
        itemService.delete(id);
        return HttpStatus.OK;
    }

    @GetMapping("/items/search")
    public List<ResponseItemDto> searchItems(@RequestParam("itemStatus") ItemStatus itemStatus) {
        return ResponseUtils.mapItemListToResponseItemDtoList(itemService.findAllItemsByStatus(itemStatus));
    }

    private Item updateItemFromRequestItemDto(Item dbItem, RequestItemDto requestItemDto) {
        if (requestItemDto.getName() != null) {
            dbItem.setName(requestItemDto.getName());
        }
        if (requestItemDto.getDescription() != null) {
            dbItem.setDescription(requestItemDto.getDescription());
        }
        if (requestItemDto.getItemStatus() != null) {
            dbItem.setItemStatus(requestItemDto.getItemStatus());
            if (requestItemDto.getItemStatus().equals(ItemStatus.IN_PROGRESS)) {
                dbItem.setStartDate(new Date());
            }
            if (requestItemDto.getItemStatus().equals(ItemStatus.DONE)) {
                dbItem.setEndDate(new Date());
            }
        }
        return dbItem;
    }

}
