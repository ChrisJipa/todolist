import {Component, OnInit} from '@angular/core';
import {ItemService} from '../item.service';
import {Observable} from 'rxjs';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-create-item',
  templateUrl: './create-item.component.html',
  styleUrls: ['./create-item.component.css']
})
export class CreateItemComponent implements OnInit {

  translationMap: Record<string, string> = {
    NOT_STARTED: 'To do',
    IN_PROGRESS: 'In progress',
    DONE: 'Done'
  };

  item: Item = {
    name: '', // Default values
    description: '',
    itemStatus: ItemStatus.TODO // Default values
  };

  statusArray: ItemStatus[] = [
    ItemStatus.TODO, ItemStatus.DONE, ItemStatus.IN_PROGRESS
  ];

  itemsArray: Observable<ItemResponse[]> | undefined;

  constructor(private itemService: ItemService, private toastrService: ToastrService) {
  }

  ngOnInit(): void {
    this.itemsArray = this.itemService.getAll();
  }

  save(): void {
    this.itemService.createItem(this.item).subscribe(data => {
      console.log(data);
      this.toastrService.success('Item has been created with success');
    }, error => {
      this.toastrService.error('Something went wrong');
    });
    // console.log('Saving item with name ' + this.item.name + ' and status ' + this.item.itemStatus);
  }

  loadItems(): void {
    this.itemService.getAll().subscribe(data => {
      console.log(data);
    });
  }

}

export interface Item {
  name: string;
  itemStatus: ItemStatus;
  description: string;
}

export enum ItemStatus {
  TODO = 'NOT_STARTED',
  IN_PROGRESS = 'IN_PROGRESS',
  DONE = 'DONE'
}

export interface ItemResponse {
  id: number;
  name: string;
  itemStatus: ItemStatus;
  description: string;
  duration: string;
}
