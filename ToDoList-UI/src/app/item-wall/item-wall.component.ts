import {Component, Inject, OnInit} from '@angular/core';
import {CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';
import {ItemResponse, ItemStatus} from '../create-item/create-item.component';
import {ItemService} from '../item.service';
import {ToastrService} from 'ngx-toastr';
import {MAT_DIALOG_DATA, MatDialog} from '@angular/material/dialog';

@Component({
  selector: 'app-item-wall',
  templateUrl: './item-wall.component.html',
  styleUrls: ['./item-wall.component.css']
})
export class ItemWallComponent implements OnInit {

  constructor(private itemService: ItemService, private toastrService: ToastrService, private matDialog: MatDialog) {
  }

  itemToDoResponse: ItemResponse[] = [];
  itemInProgressResponse: ItemResponse[] = [];
  itemDoneResponse: ItemResponse[] = [];

  ngOnInit(): void {
    this.getItemData();
  }

  getItemData(): void {
    this.itemToDoResponse = [];
    this.itemInProgressResponse = [];
    this.itemDoneResponse = [];

    this.itemService.getAll().subscribe((data: ItemResponse[]) => {
      data.forEach((item: ItemResponse) => {
        if (item.itemStatus === ItemStatus.TODO) {
          this.itemToDoResponse.push(item);
        } else if (item.itemStatus === ItemStatus.IN_PROGRESS) {
          this.itemInProgressResponse.push(item);
        } else if (item.itemStatus === ItemStatus.DONE) {
          this.itemDoneResponse.push(item);
        }
      });
      this.toastrService.success('Successfully received data!');
      console.log('TO DO: ', this.itemToDoResponse);
      console.log('IN PROGRESS: ', this.itemInProgressResponse);
      console.log('DONE: ', this.itemDoneResponse);
    }, error => {
      this.toastrService.error('Something went wrong!');
    });
  }

  drop(event: CdkDragDrop<ItemResponse[]>): void {
    console.log(event);
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      const draggedItemResponse: ItemResponse = event.previousContainer.data[event.previousIndex];
      console.log('Moving element from ' + event.previousContainer.id + 'to ' + event.container.id);
      console.log('Dragged item response: ', draggedItemResponse);

      // apelam metoda cu obiectul pe care il schimbam si id-ul container-ului in care o sa ajunga
      this.changeStatus(draggedItemResponse, event.container.id);
      this.itemService.updateStatus(draggedItemResponse.id, draggedItemResponse.itemStatus).subscribe((data: ItemResponse) => {

        this.itemDoneResponse.forEach((el: ItemResponse) => {
          if (el.id === data.id) {
            el.duration = data.duration;
          }
        });

        this.toastrService.info('Status update successful!');
      }, error => {
        this.toastrService.error(error);
      });
      console.log('Dragged Item Response with updated status: ', draggedItemResponse);
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
    }
  }

  changeStatus(itemResponse: ItemResponse, containerId: string): ItemResponse {
    if (containerId === 'TO_DO') {
      itemResponse.itemStatus = ItemStatus.TODO;
    } else if (containerId === 'In_progress') {
      itemResponse.itemStatus = ItemStatus.IN_PROGRESS;
    } else if (containerId === 'Done') {
      itemResponse.itemStatus = ItemStatus.DONE;
    }
    return itemResponse;
  }

  showDescription(item: ItemResponse): void {
    this.matDialog.open(ItemDescriptionDialogComponent, {
      data: {
        description: item.description,
        duration: item.duration
      }
    });
  }

  showDeleteDialog(id: number): void {
    const dialog = this.matDialog.open(ItemDeleteDialogComponent, {
      data: {
        itemID: id
      }
    });
    dialog.afterClosed().subscribe(el => {
      this.getItemData();
    });
  }
}

@Component({
    selector: 'app-item-description-dialog',
    templateUrl: 'item-description-dialog.html'
  }
)
export class ItemDescriptionDialogComponent {
  constructor(@Inject(MAT_DIALOG_DATA) public data: any) {
  }
}

@Component({
    selector: 'app-item-delete-dialog',
    templateUrl: 'item-delete-dialog.html'
  }
)

export class ItemDeleteDialogComponent {
  constructor(@Inject(MAT_DIALOG_DATA) public data: any, private itemService: ItemService, private toastrService: ToastrService) {
  }

  handleDelete(): void {
    /**
     * subscribe -> rezulta 2 posibile actiuni
     * 1. actiunea de succes
     * 2. actiunea de eroare
     *
     * .subscribe (data-de-la-api) => {
     *   //cum se proceseaza datele
     * }, (eroare-de-la-API) => {
     *   //cum se proceseaza eroarea
     * }
     */
    this.itemService.deleteItem(this.data.itemID).subscribe((apiData: any) => {
      this.toastrService.success('Status from API is ' + apiData);
    }, error => {
      this.toastrService.error('Status from API ' + error);
    });
  }

  handleClose(): void {
    console.log('Clicked on close');
  }
}


